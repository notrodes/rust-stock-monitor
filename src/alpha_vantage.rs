use std::collections::HashMap;

use reqwest::Url;
use serde::{Deserialize, Serialize};
use serde_json::{self, Value};

use super::RSMError;
enum Function {
	Quote
}

#[derive(Debug, Deserialize)]
pub struct MetaData {
	#[serde(rename = "1. Information")]
	information: String,
	#[serde(rename = "2. Symbol")]
	symbol: String,
	#[serde(rename = "3. Last Refreshed")]
	last_refresh: String,
	#[serde(rename = "4. Interval")]
	interval: String,
	#[serde(rename = "5. Output Size")]
	output_size: String,
	#[serde(rename = "6. Time Zone")]
	time_zone: String
}

#[derive(Debug, Deserialize)]
pub struct TimeSeries {
	#[serde(rename(deserialize = "1. open"))]
	open: f64,
	#[serde(rename(deserialize = "2. high"))]
	high: f64,
	#[serde(rename(deserialize = "3. low"))]
	low: f64,
	#[serde(rename(deserialize = "4. close"))]
	close: f64,
	#[serde(rename(deserialize = "5. volume"))]
	volume: u32
}
#[derive(Debug, Deserialize)]
pub struct IntradayData {
	#[serde(rename(deserialize = "Meta Data"))]
	meta_data: MetaData,
	#[serde(rename = "Time Series (5min)")]
	// #[serde(skip)]
	time_series: HashMap<String, TimeSeries>
}
#[derive(Clone, Debug, Deserialize, Eq, PartialEq, Serialize)]
pub struct Quote {
	#[serde(alias = "01. symbol")]
	pub symbol: String,
	#[serde(alias = "02. open")]
	pub open: String,
	#[serde(alias = "03. high")]
	pub high: String,
	#[serde(alias = "04. low")]
	pub low: String,
	#[serde(alias = "05. price")]
	pub price: String,
	#[serde(alias = "06. volume")]
	pub volume: String,
	#[serde(alias = "07. latest trading day")]
	pub latest_trading_day: String,
	#[serde(alias = "08. previous close")]
	pub previous_close: String,
	#[serde(alias = "09. change")]
	pub change: String,
	#[serde(alias = "10. change percent")]
	pub change_percent: String
}

async fn get_value(
	function: Function,
	symbol: &str
) -> Result<Value, RSMError> {
	Ok(serde_json::from_str(
		&reqwest::get(Url::parse_with_params(
			"https://www.alphavantage.co/query?",
			&[
				(
					"function",
					match function {
						Function::Quote => "GLOBAL_QUOTE"
					}
				),
				("symbol", symbol),
				("apikey", "r")
			]
		)?)
		.await?
		.text()
		.await?
	)?)
}

/// Just use "r" as api key
pub async fn get_quote(symbol: &str) -> Result<Quote, RSMError> {
	Ok(serde_json::from_value(
		get_value(Function::Quote, symbol).await?["Global Quote"].take()
	)?)
}

// mod test {
// 	use std::error::Error;

// 	use tokio::runtime::Runtime;

// 	use crate::alpha_vantage::get_quote;

// 	// const KEY: &str = "YGFYU8L3W11DER7X";

// 	#[test]
// 	fn test_quote() {
// 		Runtime::new().unwrap().block_on(get_quote("goog")).unwrap();
// 	}
// }
