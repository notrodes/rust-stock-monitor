pub mod alpha_vantage;
pub mod portfolio;

use std::io::Error as IOError;

use reqwest::Error as ReqwestError;
use rustbreak::RustbreakError;
use serde_json::Error as JSONError;
use thiserror::Error;
use url::ParseError;

#[derive(Error, Debug)]
pub enum RSMError {
	#[error("I/O error.")]
	IOError(#[from] IOError),
	#[error("Network error.")]
	ReqwestError(#[from] ReqwestError),
	#[error("Error parsing recieved data.")]
	JSONError(#[from] JSONError),
	#[error("Error parsing portfolio.")]
	RonError(#[from] RustbreakError),
	#[error("Error parsing stock symbol.")]
	UrlParseError(#[from] ParseError)
}
