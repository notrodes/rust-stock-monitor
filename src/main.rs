use clap::{arg, command};
use rust_stock_monitor::{portfolio::Portfolio, RSMError};

// TODO: removal from portfolio, number of stocks and total gain loss
#[tokio::main]
async fn main() -> Result<(), RSMError> {
	let matches = command!()
		.about("Portfolio tool that monitors stocks using Alpha Vantage API.")
		.arg(arg!(Key: -k --key [KEY] "Alpha Vantage API key"))
		.arg(arg!(Add: -a --add [SYMBOL] ... "Add stock symbols to track"))
		.get_matches();

	// TODO: err handle this
	// let _key = matches.value_of("KEY").unwrap();

	let mut portfolio = Portfolio::load("/tmp/dd")?;

	if let Some(symbols) = matches.get_many::<String>("Add") {
		for symbol in symbols {
			portfolio.insert(symbol).await?;
		}
	};

	portfolio.update().await?;
	println!("{}", portfolio);
	Ok(portfolio.save()?)
}
