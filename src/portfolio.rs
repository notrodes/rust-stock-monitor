use std::{
	collections::HashMap,
	error::Error,
	fmt::{Display, Formatter, Result as DisplayResult},
	path::Path
};

use rustbreak::{
	backend::PathBackend, deser::Ron, Database, PathDatabase, RustbreakError
};

use crate::{
	alpha_vantage::{get_quote, Quote},
	RSMError
};
// use ron::{de, ser};
/// This is the schema for the database
// Database, vec of quotes, path backend, stored useing ron serde
#[derive(Debug)]
pub struct Portfolio(Database<HashMap<String, Quote>, PathBackend, Ron>);

impl Portfolio {
	/// Loads portfolio from path or creates new portfolio with default
	pub fn load(path: impl AsRef<Path>) -> Result<Self, RustbreakError> {
		Ok(Portfolio(PathDatabase::load_from_path_or_else(
			path.as_ref().to_path_buf(),
			HashMap::new
		)?))
	}

	/// Returns Option using same rules as hashmap insert, wrapped in result for error handling io
	pub async fn insert(
		&mut self,
		symbol: &str
	) -> Result<Option<Quote>, RSMError> {
		Ok(self
			.0
			.borrow_data_mut()?
			.insert(symbol.to_owned(), get_quote(symbol).await?))
	}

	pub async fn update(&mut self) -> Result<(), RSMError> {
		for symbol in self.0.get_data(false)?.keys() {
			if let None = self.insert(symbol).await? {
				panic!("Programming mistake")
			}
		}
		Ok(())
	}

	pub fn save(&self) -> Result<(), RustbreakError> { self.0.save() }
}

impl Display for Portfolio {
	fn fmt(&self, f: &mut Formatter<'_>) -> DisplayResult {
		let quotes = self.0.borrow_data().unwrap();
		if quotes.is_empty() {
			write!(f, "Portfolio is empty.")
		} else {
			for quote in quotes.values() {
				write!(
					f,
					"{}:	Price: {}   Percent Change: {}",
					quote.symbol, quote.price, quote.change_percent
				)?
			}
			Ok(())
		}
	}
}

// impl<T: AsRef<Path>> Iterator for Portfolio<T> {
//     type Item = (String, Quote);

//     fn next(&mut self) -> Option<Self::Item> {

//     }
// }

// mod test {
// 	use crate::portfolio::Portfolio;
// 	use std::fs;
// 	use std::pin::Pin;

// 	#[test]
// 	fn test_portfolio() {
// 		let mut portfolio = Portfolio::load("notreal").unwrap();
// 		let mut insert_future = portfolio.insert("GOOG");
// 		loop {
// 			let poll = Pin::new(&mut insert_future).poll();
// 		}

// 		fs::remove_file("notreal").unwrap();
// 	}
// }
